package tp11.cuenta;

public interface Notificador {
	void notificarNuevoSaldoACliente(CuentaBancaria cuentaBancaria);

}
