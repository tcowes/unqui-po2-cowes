package tp4.supermercado;

public class Producto {
	/** Nombre del Producto. */
	private String nombre;
	
	/** Precio decimal del Producto. */ 
	protected Double precio;
	
	/** Determina si pertenece al plan de Precios Cuidados*/ 
	private boolean esPrecioCiudado = false;
	
	/** CONSTRUCTORES */
	public Producto(String nombre, Double precio, boolean precioCuidado) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCiudado = precioCuidado;
	}
	
	public Producto(String nombre, Double precio) {
		super();
		this.nombre = nombre;
		this.precio = precio;
	}
	
	public void aumentarPrecio(Double aumento) {
		this.precio += aumento;
	}
	
	/** GETTERS */
	public String getNombre() {
		return this.nombre;
	}
	
	public Double getPrecio() {
		return this.precio;
	}
	
	public boolean esPrecioCuidado() {
		return this.esPrecioCiudado;
	}
}
