package tp4.supermercado;

import java.util.ArrayList;
import java.util.List;

public class Supermercado {
	/** Nombre del Supermercado. */
	private String nombre;
	
	/** Direccion del Supermercado. */ 
	private String direccion;
	
	/** Define una lista de Productos. */
	public List<Producto> catalogo = new ArrayList<Producto>();
	
	/** CONSTRUCTOR
	 *  @param n nombre del supermercado.
	 *  @param d direccion del supermercado.
	 */
	public Supermercado(String n, String d) {
		super();
		this.nombre = n;
		this.direccion = d;
	}
	
	/** Agrega un Producto al catalogo de productos. */
	public void agregarProducto(Producto p) {
		catalogo.add(p);
	}
	
	/** Devuelve la cantidad total de productos que hay en el supermercado. */ 
	public int getCantidadDeProductos() {
		return catalogo.size();
	}
	
	/** Devuele la suma de los precios de todos los productos. */
	public Double getPrecioTotal() {
		return catalogo.stream().mapToDouble(Producto::getPrecio).sum();
	}
	
	/** GETTERS */
	public String getNombre() {
		return this.nombre;
	}
	
	public String getDireccion() {
		return this.direccion;
	}
}
