package tp4.supermercado;

public class ProductoPrimeraNecesidad extends Producto {
	private Double descuento = 10d;
	
	/** CONSTRUCTORES */
	public ProductoPrimeraNecesidad(String nombre, Double precio, boolean precioCuidado) {
		super(nombre, precio, precioCuidado);
	}
	
	public ProductoPrimeraNecesidad(String nombre, Double precio) {
		super(nombre, precio);
	}
	
	/** CONSTRUCTOR CORRESPONDIENTE AL PUNTO 2 */
	public ProductoPrimeraNecesidad(String nombre, Double precio, boolean precioCuidado, Double descuento) {
		super(nombre, precio, precioCuidado);
		this.descuento = descuento;
	}
	
	/** Modificacion del metodo getPrecio de la superclase Producto */
	@Override
	public Double getPrecio() {
		return this.precio - (this.precio * this.descuento) / 100;
	}
}