package tp4.trabajador;

import java.time.Month;

public class IngresoPorHorasExtra extends Ingreso {
	private int cantidadHoras;
	
	public IngresoPorHorasExtra(Month mes, String concepto, int monto, int horas) {
		super(mes, concepto, monto);
		this.cantidadHoras = horas;
	}
	
	@Override
	public int getMontoImponible() {
		return 0;
	}
	
	/** GETTER */
	public int getCantidadHoras() {
		return cantidadHoras;
	}
}
