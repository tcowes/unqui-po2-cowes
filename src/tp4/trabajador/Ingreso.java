package tp4.trabajador;

import java.time.Month;

public class Ingreso {
	private Month mesPercepcion;
	private String concepto;
	private int montoPercibido;
	
	public Ingreso(Month mes, String concepto, int monto) {
		super();
		this.mesPercepcion = mes;
		this.concepto = concepto;
		this.montoPercibido = monto;
	}
	
	public int getMontoPercibido() {
		return montoPercibido;
	}
	
	public int getMontoImponible() {
		return this.getMontoPercibido();
	}

	/** GETTERS */
	public Month getMesPercepcion() {
		return mesPercepcion;
	}

	public String getConcepto() {
		return concepto;
	}
}
