package tp4.trabajador;

import java.util.ArrayList;
import java.util.List;

public class Trabajador {
	private String nombre;
	private List<Ingreso> ingresosDelAnio = new ArrayList<Ingreso>();
	
	/** CONSTRUCTOR */ 
	public Trabajador(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	/** Anotar un Ingreso determinado en el array que lleva el registro de los ingresos anuales. */
	public void anotarIngreso(Ingreso ing) {
		ingresosDelAnio.add(ing);
	}
	
	/** Devuelve el monto total percibido de todos los ingresos hasta ahora. */ 
	public int getTotalPercibido() {
		return ingresosDelAnio.stream().mapToInt(Ingreso::getMontoPercibido).sum();
	}
	
	/** Devuelve el monto total imponible de todos los ingresos hasta ahora. */ 
	public int getMontoImponible() {
		return ingresosDelAnio.stream().mapToInt(Ingreso::getMontoImponible).sum();
	}
	
	/** Devuelve el total de impuestos por trabajador a pagar. */ 
	public int getImpuestoAPagar() {
		return (int) (this.getMontoImponible() * 0.02);
	}
	
	/** GETTER */
	public String getNombre() {
		return nombre;
	}
}
