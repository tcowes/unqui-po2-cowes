package tp6.bancoyprestamos;

public class SolicitudCreditoHipotecario extends SolicitudDeCredito {
	
	public SolicitudCreditoHipotecario(int monto, int plazo) {
		super(monto, plazo);
	}
	
	public boolean esAceptable() {
		return (this.montoCuotaMensual() <= solicitante.getSueldoNetoMensual() * 0.5) && 
				(this.montoSolicitado <= solicitante.getPropiedadDeGarantia().valorFiscal * 0.7) && 
				(solicitante.getEdad() + (plazo / 12) <= 65);
	}
	// Se podr�a calcular mejor la edad limite.
}
