package tp6.bancoyprestamos;

public abstract class SolicitudDeCredito {
	protected int montoSolicitado;
	protected int plazo;
	protected Cliente solicitante;
	
	public SolicitudDeCredito(int monto, int plazo) {
		this.montoSolicitado = monto;
		this.plazo = plazo;
	}
	
	public int montoCuotaMensual() {
		return this.montoSolicitado / this.plazo;
	}
	
	public Cliente getSolicitante() {
		return this.solicitante;
	}
	
	public void setSolicitante(Cliente solicitante) {
		this.solicitante = solicitante;
	}
	
	/** Va a depender del tipo de solicitud */
	public abstract boolean esAceptable();
	
	public int getMontoSolicitado() {
		return this.montoSolicitado;
	}
}
