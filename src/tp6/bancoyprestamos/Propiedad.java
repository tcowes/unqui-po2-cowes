package tp6.bancoyprestamos;

public class Propiedad {
	String descripcion;
	String direccion;
	int valorFiscal;

	public Propiedad(String descripcion, String direccion, int valor) {
		this.descripcion = descripcion;
		this.direccion = direccion;
		this.valorFiscal = valor;
	}
}

