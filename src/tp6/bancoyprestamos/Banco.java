package tp6.bancoyprestamos;

import java.util.ArrayList;

public class Banco {
	private String nombre;
	private ArrayList<Cliente> carteraClientes = new ArrayList<Cliente>();
	private ArrayList<SolicitudDeCredito> solicitudes = new ArrayList<SolicitudDeCredito>();
	
	
	public Banco(String nombre) {
		this.setNombre(nombre);
	}
	
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	/** Se registra una solicitud en la lista de solicitudes guardada, a�n no son evaluadas
	 * para saber si son aceptables, pero al momento de calcular el montoTotalADesembolsar la 
	 * misma solicitud se encargar� de responder si es v�lida para el cliente que la solicita. 
	 * */
	public void registrarSolicitud(SolicitudDeCredito solicitud, Cliente solicitante) {
		solicitud.setSolicitante(solicitante);
		this.solicitudes.add(solicitud);
		this.agregarCliente(solicitante);
	}
	
	/** Incorpora un nuevo cliente a la carteraClientes del banco. Los clientes pueden registrarse
	 * de esta forma o cuando ellos solicitan un cr�dito (no es necesario que �nicamente una persona
	 * solicite un credito para convertirse en cliente de un Banco).
	 * */
	public void agregarCliente(Cliente nuevoCliente) {
		this.carteraClientes.add(nuevoCliente);
	}
	
	public int montoTotalADesembolsar() {
		return this.solicitudes.stream()
				.filter(solicitud -> solicitud.esAceptable())
				.mapToInt(solicitud -> solicitud.getMontoSolicitado())
				.sum();
	}
	
	public int cantidadDeClientes() {
		return this.carteraClientes.size();
	}
	
	public int cantidadDeSolicitudes() {
		return this.solicitudes.size();
	}
}
