package tp6.bancoyprestamos;

public class SolicitudCreditoPersonal extends SolicitudDeCredito {
	
	public SolicitudCreditoPersonal(int monto, int plazo) {
		super(monto, plazo);
	}
	
	public boolean esAceptable() {
		return (solicitante.sueldoNetoAnual() >= 15000) && 
				(this.montoCuotaMensual() <= solicitante.getSueldoNetoMensual() * 0.7);
	}
}
