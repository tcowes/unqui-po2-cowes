package tp6.bancoyprestamos;

public class Cliente {
	private String nombre;
	private String apellido;
	private String direccion;
	private int edad;
	private int sueldoNetoMensual;
	private Propiedad propiedadDeGarantia;
	
	public Cliente(String nombre, String apellido, String direccion, int edad, int sueldoMensual) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setDireccion(direccion);
		this.setEdad(edad);
		this.setSueldoNetoMensual(sueldoMensual);
	}
	
	public int sueldoNetoAnual() {
		return this.getSueldoNetoMensual() * 12;
	}
	
	public void ponerPropiedadEnGarantia(Propiedad propiedad) {
		this.propiedadDeGarantia = propiedad;
	}

	public String getNombreCompleto() {
		return this.nombre + " " + this.apellido;
	}
	
	/** GETTERS Y SETTERS */
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	private void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	private void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	private void setEdad(int edad) {
		this.edad = edad;
	}
	
	private void setSueldoNetoMensual(int sueldo) {
		this.sueldoNetoMensual = sueldo;
	}
	
	public String getDireccion() {
		return this.direccion;
	}
	
	public int getEdad() {
		return this.edad;
	}
	
	public int getSueldoNetoMensual() {
		return this.sueldoNetoMensual;
	}
	
	public Propiedad getPropiedadDeGarantia() {
		return this.propiedadDeGarantia;
	}
}
