package ar.edu.unq.po2.tp3;

import java.util.ArrayList;
import java.util.List;

public class EquipoDeTrabajo {
	private String nombre;
	private List<Persona> integrantes = new ArrayList<Persona>();

	public EquipoDeTrabajo(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;				
	}
	
	public void agregarIntegrante(Persona p) {
		integrantes.add(p);
	}
	
	public int nroIntegrantes() { return integrantes.size(); }
	
	public int promedioDeEdad() {
		return (integrantes.stream().mapToInt(Persona::getEdad).sum()) / this.nroIntegrantes();
	}
}
