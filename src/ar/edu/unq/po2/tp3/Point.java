package ar.edu.unq.po2.tp3;

public class Point {
	private int x;
	private int y;
	
	public Point(int x, int y) {
		super();
		this.setX(x);
		this.setY(y);
	}
	
	public Point() {
		super();
		this.setX(0);
		this.setY(0);
	}
	
	private void setX(int x) {
		this.x = x;
	}
	
	private void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void mover(int x, int y) {
	// Como hago para pasar por parametro una "direccion" que responda para donde mover?
		this.setX(x);
		this.setY(y);
	}
	
	public Point sumarConPunto(Point punto) {
	// Esto se puede hacer?
		int nuevaX = this.getX() + punto.getX();
		int nuevaY = this.getY() + punto.getY();
		Point nuevoPunto = new Point(nuevaX, nuevaY);
		return nuevoPunto;
	}
}
