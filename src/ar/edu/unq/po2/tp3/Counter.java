package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Counter {

	private ArrayList<Integer> contador = new ArrayList<Integer>();
	
	public Counter() {
		super();
	}

	public void addNumber(int x) {
		contador.add(x);
	}
	
	public int getEvenOcurrences() {
		int pares = 0;
		for (int i = 0; i < contador.size(); i++) {
			if (esPar(contador.get(i))) {
				pares++; 
			}
		}
		return pares;
	}
	
	public boolean esPar(int x) {
		return (x % 2 == 0);
	}
	
	public int getOddOcurrences() {
		int impares = 0;
		for (int i = 0; i < contador.size(); i++) {
			if (!esPar(contador.get(i))) {
				impares++; 
			}
		}
		return impares;
	}
	
	public int getMultipleOcurrencesOf(int x) {
		int multiplos = 0;
		for (int i = 0; i < contador.size(); i++) {
			if (esMultiploDe(x, contador.get(i))) {
				multiplos++; 
			}
		}
		return multiplos;
	}
	
	public boolean esMultiploDe(int x, int y) {
		return y % x == 0;
	}
}