package ar.edu.unq.po2.tp3;

public class Rectangulo {
	private int ancho;
	private int alto;
	public Rectangulo(Point o, int an, int al) {
		/**
		 * Deber�a controlar que ni el alto ni el ancho sean 0, ni que sean iguales entre si.
		 */
		super();
		alto = al;
		ancho = an;
	}
	
	public int area() {
		return ancho * alto;
	}
	
	public int perimetro() {
		return 2 * (ancho + alto);
	}
	
	public String esHorizontalOVertical() {
		if (ancho > alto) { return "Horizontal"; }
		else { return "Vertical"; }
	}
}
