package ar.edu.unq.po2.tp3;

import java.time.Period;
import java.time.LocalDate;

public class Persona {
	private String nombre;
	private LocalDate fechaDeNacimiento;
	
	public Persona(String nombre, LocalDate fechaNac) {
		super();
		this.nombre = nombre;
		this.fechaDeNacimiento = fechaNac;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public LocalDate getFechaDeNacimiento() {
		return this.fechaDeNacimiento;
	}
	
	public int getEdad() {
		LocalDate fechaActual = LocalDate.now();
		return Period.between(this.fechaDeNacimiento, fechaActual).getYears();
	}
	
	public boolean menorQue(Persona otraPersona) {
		return this.getEdad() < otraPersona.getEdad();
 	}
}
