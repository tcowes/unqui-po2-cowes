package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Multioperador {

	private ArrayList<Integer> numeros = new ArrayList<Integer>();
	
	public Multioperador() {
		super();
	}
	
	public void addNumber(int x) {
		numeros.add(x);
	}
	
	public int suma() {
		int sum = 0;
		for(int i = 0; i < numeros.size(); i++)
		    sum += numeros.get(i);
		return sum;
	}
	
	public int resta() {
		int sum = 0;
		for(int i = 0; i < numeros.size(); i++)
		    sum -= numeros.get(i);
		return sum;
	}
	
	public int multiplicacion() {
		int sum = 0;
		for(int i = 0; i < numeros.size(); i++)
		    sum *= numeros.get(i);
		return sum;
	}
}
