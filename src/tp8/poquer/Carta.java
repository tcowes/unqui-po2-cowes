package tp8.poquer;

public class Carta {
	private Valor valor;
	private Palo palo;
	
	public Carta(Valor valor, Palo palo) {
		this.valor = valor;
		this.palo = palo;
	}
	
	enum Valor {
		DOS, TRES, CUATRO, CINCO, SEIS, SIETE, OCHO, NUEVE, DIEZ, JOKER, REINA, REY, AS
	}
	
	enum Palo {
		CORAZONES, DIAMANTES, PICAS, TREBOLES
	}
	
	public Valor getValor() {
		return this.valor;
	}
	
	public Palo getPalo() {
		return this.palo;
	}
}
