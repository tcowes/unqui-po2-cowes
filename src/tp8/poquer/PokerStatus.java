package tp8.poquer;

import java.util.Arrays;
import java.util.List;

public class PokerStatus {
	PokerComparator comparador = new PokerComparator();

	public String verificar(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		List<Carta> cartas = Arrays.asList(carta1, carta2, carta3, carta4, carta5);  
		return comparador.compararMano(cartas);
	}
	
	public boolean sonDelMismoPalo(Carta carta1, Carta carta2) {
		return carta1.getPalo() == carta2.getPalo();
	}
	
	public boolean esSuperior(Carta carta1, Carta carta2) {
		return carta1.getValor().ordinal() > carta2.getValor().ordinal();
	}
}
