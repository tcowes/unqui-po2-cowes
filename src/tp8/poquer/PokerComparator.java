package tp8.poquer;

import java.util.List;
import java.util.stream.Collectors;

public class PokerComparator {
	
	public String compararMano(List<Carta> manoDeCartas) {
		String respuesta;
		if (this.laManoTieneXCartas(manoDeCartas, 4)) { respuesta = "Poker"; } 
		else if (this.laManoTieneXCartas(manoDeCartas, 3)) { respuesta = "Trio"; } 
		else if (this.esManoColor(manoDeCartas)) { respuesta = "Color"; } 
		else { respuesta = "Nada"; }
		return respuesta;
	}

	private boolean esManoColor(List<Carta> manoDeCartas) {
		return manoDeCartas.stream().allMatch(carta -> carta.getPalo() == manoDeCartas.get(0).getPalo());
	}

	private boolean laManoTieneXCartas(List<Carta> manoDeCartas, int cantidadEsperada) {
		return manoDeCartas.stream().anyMatch(carta -> this.ocurrenciasDelValorDeCarta(carta, manoDeCartas) >= cantidadEsperada);
	}
	
	private int ocurrenciasDelValorDeCarta(Carta cartaAComparar, List<Carta> mano) {
		return mano.stream().filter(carta -> carta.getValor() == cartaAComparar.getValor()).collect(Collectors.toList()).size();
	}
}
