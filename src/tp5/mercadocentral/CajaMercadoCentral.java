package tp5.mercadocentral;

import java.util.List;

public class CajaMercadoCentral {
	
	public CajaMercadoCentral() {}
	
	public Double montoTotalAPagar(List<Bien> carrito) {
		Double montoTotal = 0d;
		for (Bien bien : carrito) {
			bien.registrarPago();
			montoTotal += bien.getMontoAPagar();
		}
		return montoTotal;
	}
}
