package tp5.mercadocentral;

public abstract class Producto implements Bien {
	private String nombre;
	private int stock;
	protected double precioBase;
	
	public Producto(String nombre, int stock, double precio) {
		super();
		this.nombre = nombre;
		this.stock = stock;
		this.precioBase = precio;
	}
	
	/** La precondici�n ser�a que el producto se pueda registrar, es decir, 
	 * que haya stock disponible. */
	public void registrarPago() {
		this.decrementarStock();
	}
	
	private void decrementarStock() {
		this.stock--;
	}
	
	/** GETTERS */
	public String getNombre() {
		return this.nombre;
	}
	
	public int getStock() {
		return this.stock;
	}
	
	public double getMontoAPagar() {
		return this.precioBase;
	}
}
