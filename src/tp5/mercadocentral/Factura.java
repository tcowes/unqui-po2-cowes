package tp5.mercadocentral;

public abstract class Factura implements Bien {
	private String concepto;
	private boolean boletaPaga = false;
	
	public Factura(String concepto) {
		super();
		this.concepto = concepto;
	}
	
	public abstract double getMontoAPagar();
	
	public String getConcepto() {
		return this.concepto;
	}
	
	public void registrarPago() {
		this.boletaPaga = true;
	}
	
	public boolean getBoletaPaga() {
		return this.boletaPaga;
	}
}
