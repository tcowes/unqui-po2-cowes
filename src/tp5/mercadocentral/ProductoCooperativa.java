package tp5.mercadocentral;

public class ProductoCooperativa extends Producto {

	public ProductoCooperativa(String nombre, int stock, Double precio) {
		super(nombre, stock, precio);
	}
	
	public double getMontoAPagar() {
		return this.precioBase * 0.9;
	}
}
