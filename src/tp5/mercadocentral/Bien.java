package tp5.mercadocentral;

public interface Bien {

	public double getMontoAPagar();
	public void registrarPago();
}
