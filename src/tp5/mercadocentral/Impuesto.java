package tp5.mercadocentral;

public class Impuesto extends Factura {
	private double tasaDeServicio;
	
	public Impuesto(String concepto, double tasa) {
		super(concepto);
		this.tasaDeServicio = tasa;
	}
	
	public double getMontoAPagar() { 
		return this.tasaDeServicio;
	}
}
