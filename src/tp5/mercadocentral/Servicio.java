package tp5.mercadocentral;

public class Servicio extends Factura {
	private int costoPorUnidad;
	private int unidadesConsumidas;
	
	public Servicio(String concepto, int costoUnidad, int cantUnidades) {
		super(concepto);
		this.costoPorUnidad = costoUnidad;
		this.unidadesConsumidas = cantUnidades;
	}
	
	public double getMontoAPagar() {
		return this.costoPorUnidad * this.unidadesConsumidas;
	}
}
