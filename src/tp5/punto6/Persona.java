package tp5.punto6;

import java.time.LocalDate;
import java.time.Period;

public class Persona implements SerVivo {
	private String nombre;
	private LocalDate fechaDeNacimiento;
	
	public Persona(String nombre, LocalDate fechaNac) {
		super();
		this.nombre = nombre;
		this.fechaDeNacimiento = fechaNac;
	}
	
	public LocalDate getFechaDeNacimiento() {
		return this.fechaDeNacimiento;
	}
	
	public int getEdad() {
		LocalDate fechaActual = LocalDate.now();
		return Period.between(this.fechaDeNacimiento, fechaActual).getYears();
	}
	
	public String getName() {
		return this.nombre;
	}
	
	public boolean esMenorQue(Persona otraPersona) {
		return this.getEdad() < otraPersona.getEdad();
	}
}
