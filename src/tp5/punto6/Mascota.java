package tp5.punto6;

public class Mascota implements SerVivo {
	private String nombre;
	private String raza;
	
	public Mascota(String nombre, String raza) {
		super();
		this.nombre = nombre;
		this.raza = raza;
	}
	
	public String getName() {
		return this.nombre;
	}
	
	public String getRaza() {
		return this.raza;
	}
}
