package tp5.punto6;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static Persona tomas = new Persona("Tom�s", LocalDate.parse("1997-11-24"));
	public static Persona bianca = new Persona("Bianca", LocalDate.parse("2000-07-07"));
	public static Mascota quinoa = new Mascota("Quinoa", "Gato callejero");
	public static Mascota teo = new Mascota("Teo", "Caniche");
	
	public static List<SerVivo> seresVivos = new ArrayList<SerVivo>();
	
	/** En este espacio respondo las preguntas teoricas que se hacen en el ejercicio, luego de instanciar dos Personas
	 *  y dos Mascotas las puedo meter en un ArrayList de SerVivo (interface), las dos clases tienen distinto tipo pero
	 *  implementan la misma interfaz, por lo tanto no es necesario distinguirlas al momento de iterar la lista. A su vez,
	 *  ambas pueden responder el mensaje getName porque son polimorficas. 
	 *  
	 *  Esto tambien podr�a pasar si SerVivo fuese una clase abstracta, y tanto Persona como Mascota fueran sus subclases, pero 
	 *  como se menciona en el punto 5, esto ya no ser�a posible si alguna de las dos clases fuese subclase de una clase a la que 
	 *  no se puede acceder. Es por eso que recurro al uso de una interfaz. */
	
	public static void main(String[] args) {
		seresVivos.add(tomas);
		seresVivos.add(bianca);
		seresVivos.add(quinoa);
		seresVivos.add(teo);
		for (SerVivo serVivo : seresVivos) {
			System.out.println(serVivo.getName());
		}
	}
}
