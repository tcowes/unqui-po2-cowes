package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RectanguloTestCase {
	Point origen = new Point();
	Rectangulo rectangulo1 = new Rectangulo(origen, 4, 5);
	Rectangulo rectangulo2 = new Rectangulo(origen, 3, 2);
	Rectangulo rectangulo3 = new Rectangulo(origen, 2, 5);
	
	
	@Test
	void testArea() {
		assertEquals(20, rectangulo1.area());
	}

	@Test
	void testPerimetro() {
		assertEquals(14, rectangulo3.perimetro());
	}
	
	@Test
	void esHorizontal() {
		assertEquals("Horizontal", rectangulo2.esHorizontalOVertical());
	}
}
