package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EquipoDeTrabajoTestCase {
	Persona tomas = new Persona("Tomas", LocalDate.parse("1997-11-24"));
	Persona nicolas = new Persona("Nicolas", LocalDate.parse("1993-06-03"));
	Persona ignacio = new Persona("Ignacio", LocalDate.parse("1998-12-19"));
	Persona bianca = new Persona("Bianca", LocalDate.parse("2000-07-07"));
	Persona guido = new Persona("Guido", LocalDate.parse("1997-11-05"));
	EquipoDeTrabajo opentoys = new EquipoDeTrabajo("Open Toys");
			
	@BeforeEach
	public void setUp() throws Exception {
		opentoys.agregarIntegrante(tomas);
		opentoys.agregarIntegrante(nicolas);
		opentoys.agregarIntegrante(ignacio);
		opentoys.agregarIntegrante(bianca);
		opentoys.agregarIntegrante(guido);
	}
	
	@Test
	void testEdad() {
		assertEquals(22, tomas.getEdad());
	}

	@Test
	void testCantidadIntegrantes() {
		// Sigue fallando el array, PREGUNTAR
		assertEquals(5, opentoys.nroIntegrantes());
	}
	
	@Test
	void testPromedioEdad() {
		assertEquals(22, opentoys.promedioDeEdad());
	}
}
