package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PointTestCase {
	Point punto1 = new Point(2, 2);
	Point punto2 = new Point(1, 3);
	Point puntoOrigen = new Point();
	
	@Test
	void testMoverPunto1() {
		assertEquals(2, punto1.getX());
		punto1.mover(3, 2);
		assertEquals(3, punto1.getX());
	}

	@Test
	void sumarPuntoOrigenYPunto2() {
		Point puntoSumado = punto2.sumarConPunto(puntoOrigen);
		assertEquals(3, puntoSumado.getY());
	}
}
