package ar.edu.unq.po2.tp3;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MultiopTestCase {
	private Multioperador multiop;

	@BeforeEach
	public void setUp() throws Exception {
		multiop = new Multioperador();
	
		multiop.addNumber(2);
		multiop.addNumber(3);
		multiop.addNumber(1);
		multiop.addNumber(1);
		multiop.addNumber(1);
		
	}
		
	@Test
	public void testSuma() {
			assertEquals(multiop.suma(), 8);
	}

}

