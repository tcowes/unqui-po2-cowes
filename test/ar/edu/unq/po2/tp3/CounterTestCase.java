package ar.edu.unq.po2.tp3;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CounterTestCase {
	private Counter counter;
	
	/**
	 * Crea un escenario de test b�sico, que consiste en un contador
	 * con 10 enteros
	 * @throws Exception
	 */
	@Before
	
	public void setUp() throws Exception {
		// Se crea el contador
		counter = new Counter();
		
		// Se agregaron los numeros. Un solo par y nueve impares
		counter.addNumber(1);
		counter.addNumber(3);
		counter.addNumber(5);
		counter.addNumber(7);
		counter.addNumber(9);
		counter.addNumber(1);
		counter.addNumber(1);
		counter.addNumber(1);
		counter.addNumber(1);
		counter.addNumber(4);
	}
	
	@Test
	public void testEvenNumbers() {
		
		// Obtengo la cantidad de pares
		int amount = counter.getEvenOcurrences();
		
		// Chequeo que sea la cantidad esperada
		assertEquals(amount, 1);
	}
	
	@Test
	public void testOddNumbers() {
		
		// Obtengo la cantidad de impares
		int amount = counter.getOddOcurrences();
		
		// Chequeo que sea la cantidad esperada
		assertEquals(amount, 9);
	}
	
	@Test
	public void testMultiples() {
		
		// Obtengo la cantidad de multiplos de 2
		int amount = counter.getMultipleOcurrencesOf(2);
		
		// Chequeo que sea la cantidad esperada
		assertEquals(amount, 1);
	}
}