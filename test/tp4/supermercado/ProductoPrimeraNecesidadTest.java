package tp4.supermercado;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductoPrimeraNecesidadTest {

	private ProductoPrimeraNecesidad leche;
	private ProductoPrimeraNecesidad cerveza;
	
	@BeforeEach
	public void setUp() {
		leche = new ProductoPrimeraNecesidad("Leche", 8d, false);
		cerveza = new ProductoPrimeraNecesidad("Cerveza", 100d, false, 50d);
	}
	
	@Test
	public void testCalcularPrecio() {
		assertEquals(7.2, leche.getPrecio());
	}
	
	@Test
	public void testCalcularPrecioConDescuentoModificado() {
		assertEquals(50d, cerveza.getPrecio());
	}
}