package tp4.trabajador;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Month;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TrabajadorTestCase {

	/** Creo dos trabajadores. */
	private Trabajador tomas = new Trabajador("Tomas");
	private Trabajador mariano = new Trabajador("Mariano");
	
	/** Creo 8 ingresos, todos en meses distintos salvo 2. Y 2 de los 8 son por horas extra. */ 
	private Ingreso enero = new Ingreso(Month.JANUARY, "aguinaldo", 32000);
	private Ingreso febrero = new Ingreso(Month.FEBRUARY, "sueldo", 16000);
	private Ingreso marzo = new Ingreso(Month.MARCH, "sueldo", 16500);
	private Ingreso marzoExtra = new IngresoPorHorasExtra(Month.MARCH, "sueldo", 20000, 4);
	private Ingreso abril = new Ingreso(Month.APRIL, "sueldo", 16000);
	private Ingreso abril2 = new Ingreso(Month.APRIL, "sueldo", 17000);
	private Ingreso mayo = new Ingreso(Month.MAY, "sueldo", 16000);
	private Ingreso mayoExtra = new IngresoPorHorasExtra(Month.MAY, "sueldo", 25000, 7);
	
	@BeforeEach
	public void setUp() {
		
		/** Anoto los ingresos de cada trabajador:
		*   � Tomas llega a anotar hasta mayo y hace horas extra en marzo.
		*   � Mariano se incorpora en febrero y trabaja hasta mayo, hace horas extra ese ultimo mes. */
		tomas.anotarIngreso(enero);
		tomas.anotarIngreso(febrero);
		tomas.anotarIngreso(marzoExtra);
		tomas.anotarIngreso(abril2);
		tomas.anotarIngreso(mayo);
		
		mariano.anotarIngreso(febrero);
		mariano.anotarIngreso(marzo);
		mariano.anotarIngreso(abril);
		mariano.anotarIngreso(mayoExtra);
	}
	
	@Test
	void testTotalPercibidoTomas() {
		assertEquals(101000, tomas.getTotalPercibido());
	}
	
	@Test
	void testTotalPercibidoMariano() {
		assertEquals(73500, mariano.getTotalPercibido());
	}
	
	@Test
	void testMontoImponible() {
		assertEquals(81000, tomas.getMontoImponible());
		assertEquals(48500, mariano.getMontoImponible());
	}

	@Test
	void testImpuestoAPagarTomas() {
		assertEquals(1620, tomas.getImpuestoAPagar());
	}
	
	@Test
	void testImpuestoAPagarMariano() {
		assertEquals(970, mariano.getImpuestoAPagar());
	}
}
