package tp6.bancoyprestamos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class BancoYPrestamosTestCase {
	
	Banco bancoCentral = new Banco("Banco Central");
	
	Cliente tomas = new Cliente("Tomas", "Cowes", "Belgrano 123", 22, 20000); 
	Cliente bianca = new Cliente("Bianca", "Cowes", "Belgrano 124", 20, 1100);
	Cliente eliana = new Cliente("Eliana", "Aquin", "Mitre 1000", 23, 25000);
	
	Propiedad casaFalsa = new Propiedad("Una casa super realista", "Calle Falsa 123", 60000);
	
	SolicitudCreditoPersonal sol1 = new SolicitudCreditoPersonal(13500, 16);
	SolicitudCreditoHipotecario sol2 = new SolicitudCreditoHipotecario(15000, 10);
	SolicitudCreditoPersonal sol3 = new SolicitudCreditoPersonal(20000, 9);
	
	@Test
	void testBancoRegistraSolicitudYCliente() {
		bancoCentral.registrarSolicitud(sol1, tomas);
		assertEquals(1, bancoCentral.cantidadDeClientes());
		assertEquals(1, bancoCentral.cantidadDeSolicitudes());
	}

	@Test
	void testSolicitudCreditoPersonalAceptada() {
		bancoCentral.registrarSolicitud(sol1, tomas);
		assert(sol1.esAceptable());
	}
	
	@Test
	void testSolicitudHipotecariaAceptada() {
		eliana.ponerPropiedadEnGarantia(casaFalsa);
		bancoCentral.registrarSolicitud(sol2, eliana);
		assert(sol2.esAceptable());
	}
	
	@Test
	void testMontoTotalADesembolsar() {
		bancoCentral.registrarSolicitud(sol1, tomas);
		eliana.ponerPropiedadEnGarantia(casaFalsa);
		bancoCentral.registrarSolicitud(sol2, eliana);
		bancoCentral.registrarSolicitud(sol3, bianca);
		assertEquals(28500, bancoCentral.montoTotalADesembolsar());
	}
}
