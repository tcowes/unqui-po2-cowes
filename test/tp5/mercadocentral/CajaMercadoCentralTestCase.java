package tp5.mercadocentral;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CajaMercadoCentralTestCase {
	/** MercadoCentral */
	public CajaMercadoCentral cajaMercado = new CajaMercadoCentral();
	
	/** Productos */
	public ProductoCooperativa arroz = new ProductoCooperativa("Arroz", 2, 50d);
	public ProductoCooperativa harina = new ProductoCooperativa("Harina", 3, 80d);
	public ProductoCooperativa fideos = new ProductoCooperativa("Fideos", 100, 90d);
	
	public ProductoTradicional yerba = new ProductoTradicional("Yerba", 5, 100d);
	public ProductoTradicional te = new ProductoTradicional("Te", 50, 25d);

	/** Facturas */ 
	public Servicio agua = new Servicio("Agua", 100, 8);
	public Servicio luz = new Servicio("Luz", 50, 100);
	public Servicio gas = new Servicio("Gas", 150, 4);
	
	public Impuesto abl = new Impuesto("ABL", 500d);
	public Impuesto inmobiliario = new Impuesto("Inmobiliario", 250d);
	
	/** Cajas del mercado de un cliente cualquiera */
	public List<Bien> caja1 = new ArrayList<Bien>();
	public List<Bien> caja2 = new ArrayList<Bien>();
	public List<Bien> caja3 = new ArrayList<Bien>();
	
	@BeforeEach
	public void setUp() {
		caja1.add(arroz);
		caja1.add(harina);
		caja1.add(fideos);
		caja1.add(yerba);
		caja1.add(te);
		
		caja2.add(harina);
		caja2.add(fideos);
		caja2.add(te);
		caja2.add(agua);
		caja2.add(luz);
		
		caja3.add(gas);
		caja3.add(abl);
		caja3.add(inmobiliario);
	}
	
	/** La caja1 tiene 5 items */
	@Test
	void testCarrito() {
		assertEquals(5, caja1.size());
	}

	/** El stock decrece luego de registrar el pago */
	@Test
	void testStock() {
		assertEquals(2, arroz.getStock());
		arroz.registrarPago();
		assertEquals(1, arroz.getStock());
	}
	
	/** Un servicio pasa a estar pago una vez que se lo registra */
	@Test
	void testPago() {
		assertFalse(luz.getBoletaPaga());
		luz.registrarPago();
		assertTrue(luz.getBoletaPaga());
	}
	
	/** Monto total de la caja1 */
	@Test
	void testMontoTotal() {
		assertEquals(323d, cajaMercado.montoTotalAPagar(caja1));
	}
	
	/** Monto total de la caja2 */ 
	@Test
	void testMontoTotal2() {
		assertEquals(5978d, cajaMercado.montoTotalAPagar(caja2));
	}
	
	/** Monto total de la caja3 */
	@Test
	void testMontoTotal3() {
		assertEquals(1350d, cajaMercado.montoTotalAPagar(caja3));
	}
	
}
