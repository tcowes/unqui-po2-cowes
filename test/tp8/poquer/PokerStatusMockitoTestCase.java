package tp8.poquer;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import tp8.poquer.Carta.Valor;

class PokerStatusMockitoTestCase {
    // SETUP:
    //  DOC:
    Carta as = mock(Carta.class); //dummy
    Carta relleno = mock(Carta.class); //dummy
    //  SUT:
    PokerStatus status = new PokerStatus();
    
    @BeforeEach
    public void setUp() {	
    when(as.getValor()).thenReturn(Valor.AS);
    when(relleno.getValor()).thenReturn(Valor.REY);
    }
    
    @Test
    void verificarTest() {
    // EXCERCISE:
    	status.verificar(as, as, as, as, relleno);
    // VERIFY:
    	verify(as, atLeast(1)).getValor();
    	verify(relleno, atLeast(1)).getValor();
    }

}