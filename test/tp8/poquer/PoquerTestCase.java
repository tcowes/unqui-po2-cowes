package tp8.poquer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tp8.poquer.Carta.Palo;
import tp8.poquer.Carta.Valor;

class PoquerTestCase {
	/** SETUP */ 
	PokerStatus verificadorDeManos = new PokerStatus();
	
	Carta dosPicas = new Carta(Valor.DOS, Palo.PICAS);
	Carta dosDiamantes = new Carta(Valor.DOS, Palo.DIAMANTES);
	Carta dosCorazones = new Carta(Valor.DOS, Palo.CORAZONES);
	Carta jokerTreboles = new Carta(Valor.JOKER, Palo.TREBOLES);
	Carta reyDiamantes = new Carta(Valor.REY, Palo.DIAMANTES);
	Carta cuatroPicas = new Carta(Valor.CUATRO, Palo.PICAS);
	Carta cincoTreboles = new Carta(Valor.CINCO, Palo.TREBOLES);
	Carta cincoDiamantes = new Carta(Valor.CINCO, Palo.DIAMANTES);
	Carta cincoPicas = new Carta(Valor.CINCO, Palo.PICAS);
	Carta cincoCorazones = new Carta(Valor.CINCO, Palo.CORAZONES);
	Carta reinaTreboles = new Carta(Valor.REINA, Palo.TREBOLES);
	Carta asDiamantes = new Carta(Valor.AS, Palo.DIAMANTES);
	Carta cuatroDiamantes = new Carta(Valor.CUATRO, Palo.DIAMANTES);
	Carta sieteDiamantes = new Carta(Valor.SIETE, Palo.DIAMANTES);
	
	
	/** EXCERCISE, VERIFY Y TEARDOWN en cada test realizado individualmente. */
	@Test
	void testTrio() {
		assertEquals("Trio", verificadorDeManos.verificar(dosPicas, dosDiamantes, dosCorazones, jokerTreboles, reyDiamantes));
	}

	@Test
	void testNada() {
		assertEquals("Nada", verificadorDeManos.verificar(cuatroPicas, dosDiamantes, cincoTreboles, reinaTreboles, reyDiamantes));
	}
	
	@Test
	void testPoker() {
		assertEquals("Poker", verificadorDeManos.verificar(cincoDiamantes, cincoPicas, cincoCorazones, cincoTreboles, reyDiamantes));
	}
	
	@Test
	void testColor() {
		assertEquals("Color", verificadorDeManos.verificar(dosDiamantes, cuatroDiamantes, asDiamantes, cincoDiamantes, sieteDiamantes));
	}
	
	@Test
	void sonDelMismoPalo() {
		assert(verificadorDeManos.sonDelMismoPalo(dosDiamantes, cuatroDiamantes));
	}
	
	@Test
	void noSonDelMismoPalo() {
		assertFalse(verificadorDeManos.sonDelMismoPalo(dosDiamantes, cincoTreboles));
	}
	
	@Test
	void esSuperiorElRey() {
		assert(verificadorDeManos.esSuperior(reyDiamantes, cincoCorazones));
	}
	
	@Test
	void noEsSuperiorElDos() {
		assertFalse(verificadorDeManos.esSuperior(dosCorazones, cincoCorazones));
	}
	
	@Test 
	void noHaySuperiorEntreDosCartasConElMismoValor() {
		assertFalse(verificadorDeManos.esSuperior(dosCorazones, dosDiamantes));
	}
}
